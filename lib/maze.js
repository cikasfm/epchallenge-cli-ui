const rest = require('restler');
const ctx = require('axel');

let directions = [
  'north',
  'east',
  'south',
  'west'
];

const BLOCK_SIZE_X = 2; // horizontal block size
const BLOCK_SIZE_Y = 1; // vertical block size

let directionMap = {
  east: { fx: BLOCK_SIZE_X, fy: 0, tx: BLOCK_SIZE_X, ty: BLOCK_SIZE_Y },
  west: { fx: -1, fy: 0, tx: -1, ty: BLOCK_SIZE_Y },
  north: { fx: 0, fy: -1, tx: BLOCK_SIZE_X, ty: -1 },
  south: { fx: 0, fy: BLOCK_SIZE_Y, tx: BLOCK_SIZE_X, ty: BLOCK_SIZE_Y },
}

// stack of cells with multiple moves available
let multi = [];
// total moves count
let moves = 0;

const maze = {

  loc: {
    x: 0,
    y: 0
  },

  go: function(){
    return maze.init()
    .then( maze.explore )
    .catch( console.error );
  },

  explore: function( cell ) {
    moves++;
    if ( cell.atEnd === true ) {
      ctx.cursor.restore();
      return Promise.resolve();
    } else {
      let countUE = 0;
      let direction;
      // let's count unexplored directions
      let checkDirections = function( dir ) {
        if ( cell[dir] === 'UNEXPLORED' ) {
          direction = dir;
          countUE++;
        }
      };
      directions.forEach( checkDirections );
      // if more than one - add cell to stack
      if ( countUE > 1 ) multi.push( { x: cell.x, y: cell.y, mazeGuid: cell.mazeGuid } );
      if ( countUE > 0 ) {
        // if at least one direction was unexplored - move to that ( last checked ) direction
        return maze.move( { direction: direction.toUpperCase(), mazeGuid: cell.mazeGuid } );
      } else {
        // no unexplored directions. jump back
        return maze.jump( multi.pop() );
      }
    }
  },

  color: ( cell ) => {
    directions.forEach( dir => {
      switch( cell[dir] ) {
        case 'UNEXPLORED':
          ctx.bg(0,0,255); // blue
          break;
        case 'BLOCKED':
          ctx.bg(255,0,0); // red
          break;
        case 'VISITED':
        default:
          ctx.bg(0,255,0); // green
      }
      ctx.line(
        cell.x * ( BLOCK_SIZE_X + 1 ) + directionMap[dir].fx,
        cell.y * ( BLOCK_SIZE_Y + 1 ) + directionMap[dir].fy,
        cell.x * ( BLOCK_SIZE_X + 1 ) + directionMap[dir].tx,
        cell.y * ( BLOCK_SIZE_Y + 1 ) + directionMap[dir].ty );
    } );

    // White box - new location
    ctx.bg(255,0,0);
    ctx.box( cell.x * ( BLOCK_SIZE_X + 1 ), cell.y * ( BLOCK_SIZE_Y + 1 ), BLOCK_SIZE_X, BLOCK_SIZE_Y );

    // Green box - previous location
    ctx.bg(0,255,0);
    ctx.box( maze.loc.x * ( BLOCK_SIZE_X + 1 ), maze.loc.y * ( BLOCK_SIZE_Y + 1 ), BLOCK_SIZE_X, BLOCK_SIZE_Y );

    maze.loc = {
      x: cell.x,
      y: cell.y
    };

    return cell;
  },

  move: function( data ){
    // call API to MOVE to the given direction
    return maze.api( 'move', data )
    .then( maze.color )
    .then( maze.explore );
  },

  jump: function( data ) {
    // call API to JUMP to the previous unexplored cell
    return maze.api( 'jump', data )
    .then( maze.color )
    .then( maze.explore );
  },

  init: function(){
    ctx.clear();
    return maze.api( 'init' );
  },

  api: function( path, data ) {
    let host = 'epdeveloperchallenge.com'; // TODO : extract host
    let url = 'https://' + host + '/api/' + path;

    return new Promise(function(resolve, reject) {
      let req = ( data ) ? rest.post(url, { data: data }) : rest.post(url);

      req.on('complete', function(res) {
        if (res instanceof Error) {
          reject( res );
        } else {
          resolve( res.currentCell );
        }
      });
      req.on('error', console.log )
    });
  }

};

module.exports = maze;
